module Engine where

import Color exposing (..)
import Graphics.Collage exposing (..)
import Graphics.Element exposing (..)
import Keyboard
import Time exposing (..)
import Signal exposing (..)
import Window
import Set
import Debug


-- CONSTANTS --------
keycodeZ = 90
keycodeX = 88
keycodeC = 67

keycodeDash = keycodeZ
keycodeJump = keycodeX
keycodeShoot = keycodeC

gravityForce : Float
gravityForce = 0.6

jumpForce : Float
jumpForce = 14

walkSpeed : Float
walkSpeed = 4

dashSpeed : Float
dashSpeed = 9

dashDuration : Time
dashDuration = 800 * millisecond

floorPos : Float
floorPos = 70

type alias IntVector2 = { x : Int, y : Int }
type alias KeysDownSet = Set.Set Keyboard.KeyCode

-- MODEL
type alias CharacterState = {
  x : Float,
  y : Float,
  vx : Float,
  vy : Float,
  dir : String,
  isGrounded : Bool,
  canJump : Bool,
  isDashing : Bool,
  canDash : Bool,
  dashStartTime : Time
  }

initialCharacterState : CharacterState
initialCharacterState = {
  x=0, y=floorPos,
  vx=0, vy=0,
  dir="right",
  isGrounded=True,
  canJump=True,
  isDashing=False,
  canDash=True,
  dashStartTime=0
  }

-- UPDATE functions
isJumpKeyDown : KeysDownSet -> Bool
isJumpKeyDown keysDown = Set.member keycodeJump keysDown

isDashKeyDown : KeysDownSet -> Bool
isDashKeyDown keysDown = Set.member keycodeDash keysDown

startJump : KeysDownSet -> CharacterState -> CharacterState
startJump keysDown character =
  if isJumpKeyDown keysDown && character.isGrounded && character.canJump
  then { character | vy <- jumpForce, canJump <- False }
  else character

startDash : KeysDownSet -> Time -> CharacterState -> CharacterState
startDash keysDown now character =
  if isDashKeyDown keysDown && character.isGrounded && character.canDash
  then { character |
    isDashing <- True,
    canDash <- False,
    dashStartTime <- now,
    vx <- if character.dir == "right" then dashSpeed else -dashSpeed
    }
  else character

cancelDash : KeysDownSet -> CharacterState -> CharacterState
cancelDash keysDown character =
  if (not <| isDashKeyDown keysDown) && character.isGrounded && character.isDashing
  then { character | isDashing <- False }
  else character

cancelDashWhenWalkingOpposite : IntVector2 -> CharacterState -> CharacterState
cancelDashWhenWalkingOpposite {x} character =
  if character.isGrounded && character.isDashing && character.vx * (toFloat x) < 0
  then { character | isDashing <- False }
  else character

cancelDashWithTime : Time -> CharacterState -> CharacterState
cancelDashWithTime now character =
  let
    dashTimeExpired = now - character.dashStartTime > dashDuration
  in
    if character.isGrounded && character.isDashing && dashTimeExpired
    then { character | isDashing <- False, dashStartTime <- 0 }
    else character

cancelUpwardsJump : KeysDownSet -> CharacterState -> CharacterState
cancelUpwardsJump keysDown character =
  if not (isJumpKeyDown keysDown) && not character.canJump && character.vy > 0
  then { character | vy <- 0 }
  else character

resetCanJump : KeysDownSet -> CharacterState -> CharacterState
resetCanJump keysDown character =
  if not (isJumpKeyDown keysDown)
  then { character | canJump <- True }
  else character

resetCanDash : KeysDownSet -> CharacterState -> CharacterState
resetCanDash keysDown character =
  if not (isDashKeyDown keysDown)
  then { character | canDash <- True }
  else character

dirToX : String -> Int
dirToX dir =
  if | dir == "left" -> -1
     | dir == "right" -> 1
     | otherwise -> 0

calculateVxOnGround : Int -> CharacterState -> Float
calculateVxOnGround arrowX character =
  if character.isDashing
  then dashSpeed * toFloat (dirToX character.dir)
  else walkSpeed * toFloat arrowX

calculateVxInAir : Int -> CharacterState -> Float
calculateVxInAir arrowX character =
  (if character.isDashing then dashSpeed else walkSpeed) * toFloat arrowX

moveHorizontally : IntVector2 -> CharacterState -> CharacterState
moveHorizontally {x} character = { character |
  vx <-
    if character.isGrounded
    then calculateVxOnGround x character
    else calculateVxInAir x character,
  dir <- if | x < 0 -> "left"
            | x > 0 -> "right"
            | otherwise -> character.dir }

applyGravity : Float -> CharacterState -> CharacterState
applyGravity dt character =
  if not character.isGrounded
  then { character | vy <- character.vy - dt * gravityForce }
  else character

applyMotion : Float -> CharacterState -> CharacterState
applyMotion dt character = { character |
  x <- character.x + dt*character.vx,
  y <- character.y + dt*character.vy
  }

applyCollisions : CharacterState -> CharacterState
applyCollisions character = { character |
  y <- max floorPos character.y,
  vy <- if character.y < floorPos then 0 else character.vy,
  isGrounded <- character.y <= floorPos,
  isDashing <- if character.y < floorPos then False else character.isDashing
  }

update : (Time, Float, IntVector2, KeysDownSet) -> CharacterState -> CharacterState
update (now, dt, arrows, keysDown) character =
  character |>
  startJump keysDown |>
  cancelUpwardsJump keysDown |>
  startDash keysDown now |>
  cancelDash keysDown |>
  cancelDashWhenWalkingOpposite arrows |>
  cancelDashWithTime now |>
  moveHorizontally arrows |>
  applyGravity dt |>
  applyMotion dt |>
  applyCollisions |>
  resetCanJump keysDown |>
  resetCanDash keysDown


renderCharacter w h character =
  let
    size = 100
  in
    rect size size
    |>
    filled (rgb 0 126 255)
    |>
    move (character.x, character.y + size*0.5 - h*0.5)

renderFloor w h =
  rect w floorPos |> filled (rgb 106 130 157) |> move (0, floorPos*0.5 - h*0.5)

-- DISPLAY
render (w',h') character =
  let (w,h) = (toFloat w', toFloat h')
      verb = if | character.y  >  0 -> "jump"
                | character.vx /= 0 -> "walk"
                | otherwise     -> "stand"
      src = "/imgs/character/" ++ verb ++ "/" ++ character.dir ++ ".gif"
  in collage w' h' [
    rect w h  |> filled (rgb 32 47 75)
    ,
    renderFloor w h
    ,
    renderCharacter w h (Debug.watch "character" character)
    ]

debugged_arrows = map (Debug.watch "arrows") Keyboard.arrows
debugged_keysDown = map (Debug.watch "keysDown") Keyboard.keysDown

combineInputs : (a, b) -> c -> d -> (a, b, c, d)
combineInputs (now, delta) arrows keysDown = (now, delta, arrows, keysDown)

input = let deltaAndNow = timestamp <| map (\t -> t/20) (fps 60)
        in map3 combineInputs deltaAndNow debugged_arrows debugged_keysDown

main = map2 render Window.dimensions (foldp update initialCharacterState input)
